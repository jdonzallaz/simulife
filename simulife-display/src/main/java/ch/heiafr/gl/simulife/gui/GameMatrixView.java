package ch.heiafr.gl.simulife.gui;

import ch.heiafr.gl.simulife.model.ICreature;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * This class is used to create the matrix view of the game.
 *
 * @author Jonathan Rial
 */
@SuppressWarnings("Duplicates")
public class GameMatrixView extends Application implements IGameView {

  private static final CountDownLatch latch = new CountDownLatch(1);
  private static GridPane grid;

  private static int width;
  private static int height;

  private HashMap<ICreature, Rectangle> creatures;
  private List<FunctionWithParams> delayed;

  /** This method is used to wait for the initialization of the JavaFX view. */
  private static void waitForStartUp() {
    try {
      latch.await();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  /**
   * This method creates the JavaFX view.
   *
   * @param primaryStage the primary stage, onto which the application scene can be set.
   */
  @Override
  public void start(Stage primaryStage) {
    grid = new GridPane();

    grid.setGridLinesVisible(true);

    double percentWidth = 100.0 / width;
    for (int i = 0; i < width; i++) {
      ColumnConstraints colConst = new ColumnConstraints();
      colConst.setPercentWidth(percentWidth);
      grid.getColumnConstraints().add(colConst);
    }

    double percentHeight = 100.0 / height;
    for (int i = 0; i < height; i++) {
      RowConstraints rowConst = new RowConstraints();
      rowConst.setPercentHeight(percentHeight);
      grid.getRowConstraints().add(rowConst);
    }

    primaryStage.setScene(new Scene(grid, 600, 600));
    primaryStage.show();

    latch.countDown();
  }

  /** {@inheritDoc} */
  @Override
  public void initGame(int width, int height) {
    GameMatrixView.width = width;
    GameMatrixView.height = height;
    this.creatures = new HashMap<>();
    this.delayed = new ArrayList<>();
    new Thread(() -> Application.launch(GameMatrixView.class)).start();
    GameMatrixView.waitForStartUp();
  }

  /** {@inheritDoc} */
  @Override
  public void set(ICreature creature, int x, int y) {
    delayed.add(new FunctionWithParams(this::set, new Object[] {creature, x, y}));
  }

  /**
   * This method sets a creature on the grid at a specified cell. It is stored in a
   * FunctionWithParams object and is executed when the referesh() method is called.
   *
   * @param creature the creature to set on the grid.
   * @param params the parameters as an array of Integers.
   */
  private void set(ICreature creature, Integer[] params) {
    if (isOutside(params[0], params[1])) {
      throw new RuntimeException("Adding a creature outside the game grid");
    }

    Rectangle rectangle = createRectangleFromCreature(creature);

    Platform.runLater(() -> grid.add(rectangle, params[0], params[1]));
    creatures.put(creature, rectangle);
  }

  /** {@inheritDoc} */
  @Override
  public void move(ICreature creature, int x, int y) {
    delayed.add(new FunctionWithParams(this::move, new Object[] {creature, x, y}));
  }

  /**
   * This method moves a creature on the grid to a specified cell. It is stored in a
   * FunctionWithParams object and is executed when the referesh() method is called.
   *
   * @param creature the creature to move on the grid.
   * @param params the parameters as an array of Integers.
   */
  private void move(ICreature creature, Integer[] params) {
    remove(creature, null);
    set(creature, params);
  }

  /** {@inheritDoc} */
  @Override
  public void remove(ICreature creature) {
    delayed.add(new FunctionWithParams(this::remove, new Object[] {creature}));
  }

  /**
   * This method removes a creature from the grid. It is stored in a FunctionWithParams object and
   * is executed when the referesh() method is called.
   *
   * @param creature the creature to remove from the grid.
   * @param params the parameters as an array of Integers.
   */
  @SuppressWarnings("unused")
  private void remove(ICreature creature, Integer[] params) {
    Rectangle rectangle = creatures.get(creature);

    if (rectangle == null) {
      throw new RuntimeException("Creature not found");
    }

    Platform.runLater(() -> grid.getChildren().remove(rectangle));
    creatures.remove(creature);
  }

  /** {@inheritDoc} */
  @Override
  public void refresh() {
    delayed.forEach(
        fwa -> {
          if (fwa.getParams().length > 1) {
            Integer[] pos =
                new Integer[] {(Integer) fwa.getParams()[1], (Integer) fwa.getParams()[2]};
            fwa.getFunction().accept((ICreature) fwa.getParams()[0], pos);
          } else {
            fwa.getFunction().accept((ICreature) fwa.getParams()[0], null);
          }
        });
    delayed.clear();
  }

  /**
   * This method checks if the specified cell is outside the grid.
   *
   * @param x the index of the column.
   * @param y the index of the row.
   * @return True if the the specified cell is outside the grid otherwise false.
   */
  private boolean isOutside(int x, int y) {
    return x < 0 || x >= width || y < 0 || y >= height;
  }

  /**
   * This method creates an ImageView from the creature object passed as a parameter.
   *
   * @param creature the creature object.
   * @return the ImageView created from the creature object.
   */
  private static Rectangle createRectangleFromCreature(ICreature creature) {
    Rectangle rectangle =
        new Rectangle(0, 0, grid.getWidth() * (1.0 / width), grid.getHeight() * (1.0 / height));

    rectangle.setFill(creature.getColor());

    rectangle.widthProperty().bind(grid.widthProperty().multiply(1.0 / width));
    rectangle.heightProperty().bind(grid.heightProperty().multiply(1.0 / height));

    return rectangle;
  }
}
