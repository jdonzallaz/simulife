package ch.heiafr.gl.simulife.model;

import javafx.scene.image.Image;
import javafx.scene.paint.Color;

/**
 * This interface contains all required getters for a creature.
 *
 * @author Jonathan Rial
 */
public interface ICreature {

  /**
   * Gets the name of the creature to display it in the text view of the game.
   *
   * @return the name of the creature.
   */
  String getName();

  /**
   * Gets the color of the creature to display it in the matrix view of the game.
   *
   * @return the color of the creature.
   */
  Color getColor();

  /**
   * Gets the image of the creature to display it in the image view of the game.
   *
   * @return the image of the creature.
   */
  Image getImage();
}
