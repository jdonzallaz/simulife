package ch.heiafr.gl.simulife.gui;

import ch.heiafr.gl.simulife.model.ICreature;
import javafx.application.Platform;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class GameGrid extends GridPane implements IGameGrid {
    private int rows;
    private int columns;

    private HashMap<ICreature, ImageView> creatures;
    private List<FunctionWithParams> delayed;

    @Override
    public void init(int rows, int columns) {
        this.rows = rows;
        this.columns = columns;
        this.creatures = new HashMap<>();
        this.delayed = new ArrayList<>();

        setGridLinesVisible(true);

        double percentWidth = 100.0 / columns;
        for (int i = 0; i < columns; i++) {
            ColumnConstraints colConst = new ColumnConstraints();
            colConst.setPercentWidth(percentWidth);
            getColumnConstraints().add(colConst);
        }

        double percentHeight = 100.0 / rows;
        for (int i = 0; i < rows; i++) {
            RowConstraints rowConst = new RowConstraints();
            rowConst.setPercentHeight(percentHeight);
            getRowConstraints().add(rowConst);
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void set(ICreature creature, int x, int y) {
        delayed.add(new FunctionWithParams(this::set, new Object[]{creature, x, y}));
    }

    /**
     * This method sets a creature on the grid at a specified cell. It is stored in a
     * FunctionWithParams object and is executed when the referesh() method is called.
     *
     * @param creature the creature to set on the grid.
     * @param params   the parameters as an array of Integers.
     */
    private void set(ICreature creature, Integer[] params) {
        if (isOutside(params[0], params[1])) {
            throw new RuntimeException("Adding a creature outside the game grid");
        }

        ImageView imageView = createImageViewFromCreature(creature);

        Platform.runLater(() -> add(imageView, params[0], params[1]));
        creatures.put(creature, imageView);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void move(ICreature creature, int x, int y) {
        delayed.add(new FunctionWithParams(this::move, new Object[]{creature, x, y}));
    }

    /**
     * This method moves a creature on the grid to a specified cell. It is stored in a
     * FunctionWithParams object and is executed when the referesh() method is called.
     *
     * @param creature the creature to move on the grid.
     * @param params   the parameters as an array of Integers.
     */
    private void move(ICreature creature, Integer[] params) {
        remove(creature, null);
        set(creature, params);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void remove(ICreature creature) {
        delayed.add(new FunctionWithParams(this::remove, new Object[]{creature}));
    }

    /**
     * This method removes a creature from the grid. It is stored in a FunctionWithParams object and
     * is executed when the referesh() method is called.
     *
     * @param creature the creature to remove from the grid.
     * @param params   the parameters as an array of Integers.
     */
    @SuppressWarnings("unused")
    private void remove(ICreature creature, Integer[] params) {
        ImageView imageView = creatures.get(creature);

        if (imageView == null) {
            throw new RuntimeException("Creature not found");
        }

        Platform.runLater(() -> getChildren().remove(imageView));
        creatures.remove(creature);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void refresh() {
        delayed.forEach(
                fwa -> {
                    if (fwa.getParams().length > 1) {
                        Integer[] pos = new Integer[]{(Integer) fwa.getParams()[1], (Integer) fwa.getParams()[2]};
                        fwa.getFunction().accept((ICreature) fwa.getParams()[0], pos);
                    } else {
                        fwa.getFunction().accept((ICreature) fwa.getParams()[0], null);
                    }
                });
        delayed.clear();
    }

    /**
     * This method checks if the specified cell is outside the grid.
     *
     * @param x the index of the column.
     * @param y the index of the row.
     * @return True if the the specified cell is outside the grid otherwise false.
     */
    private boolean isOutside(int x, int y) {
        return x < 0 || x >= rows || y < 0 || y >= columns;
    }

    /**
     * This method creates an ImageView from the creature object passed as a parameter.
     *
     * @param creature the creature object.
     * @return the ImageView created from the creature object.
     */
    private ImageView createImageViewFromCreature(ICreature creature) {
        ImageView imageView = new ImageView();
        imageView.setImage(creature.getImage());

        imageView.setFitWidth(getWidth() * (1.0 / rows));
        imageView.setFitHeight(getHeight() * (1.0 / columns));

        imageView.fitWidthProperty().bind(widthProperty().multiply(1.0 / rows));
        imageView.fitHeightProperty().bind(heightProperty().multiply(1.0 / columns));

        return imageView;
    }
}
