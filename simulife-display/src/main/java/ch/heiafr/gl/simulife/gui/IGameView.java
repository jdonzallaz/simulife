package ch.heiafr.gl.simulife.gui;

import ch.heiafr.gl.simulife.model.ICreature;
import javafx.scene.Parent;

/**
 * This interface contains all required methods for all game views.
 *
 * @author Jonathan Rial
 */
public interface IGameView {

  /**
   * This method initializes the game view with a specified grid size.
   *
   * @param width the width of the grid.
   * @param height the height of the grid.
   */
  void initGame(int width, int height, Parent gameRoot);

  /**
   * This method sets a creature on the grid at a specified cell.
   *
   * @param creature the creature to set on the grid.
   * @param x the index of the column.
   * @param y the index of the row.
   */
  void set(ICreature creature, int x, int y);

  /**
   * This method moves a creature on the grid to a specified cell.
   *
   * @param creature the creature to move on the grid.
   * @param x the index of the column.
   * @param y the index of the row.
   */
  void move(ICreature creature, int x, int y);

  /**
   * This method removes a creature from the grid.
   *
   * @param creature the creature to remove from the grid.
   */
  void remove(ICreature creature);

  /** This methods refreshes the game view. */
  void refresh();
}
