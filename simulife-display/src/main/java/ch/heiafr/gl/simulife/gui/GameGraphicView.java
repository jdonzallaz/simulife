package ch.heiafr.gl.simulife.gui;

import ch.heiafr.gl.simulife.model.ICreature;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.ImageView;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.RowConstraints;
import javafx.stage.Stage;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.concurrent.CountDownLatch;

/**
 * This class is used to create the graphic view of the game.
 *
 * @author Jonathan Rial
 * @author Jonathan Donzallaz
 */
@SuppressWarnings("Duplicates")
public class GameGraphicView extends Application implements IGameView {

    private static final CountDownLatch latch = new CountDownLatch(1);
    private static GridPane grid;

    private Stage primaryStage;

    /**
     * This method is used to wait for the initialization of the JavaFX view.
     */
    private static void waitForStartUp() {
        try {
            latch.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    /**
     * This method creates the JavaFX view.
     *
     * @param primaryStage the primary stage, onto which the application scene can be set.
     */
    @Override
    public final void start(Stage primaryStage) {
        this.primaryStage = primaryStage;

        Parent root = getGameRoot();

        if (grid == null)
            throw new IllegalStateException("grid field should be defined with setGridPane(grid) in getGameRoot() overrides.");



        changeScene(new Scene(root, getWidth(), getHeight()));

        latch.countDown();
    }

    public void changeScene(Scene scene) {
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Override to modify the default root component.
     *
     * @return the new root component
     */
    public Parent getGameRoot() {
        setGridPane(new GridPane());
        return grid;
    }

    /**
     * Set the gridPane which contains the creatures.
     *
     * @param gridPane the gridPane
     */
    protected final void setGridPane(GridPane gridPane) {
        grid = gridPane;
    }

    /**
     * Override to change default width
     *
     * @return scene's width
     */
    protected int getWidth() {
        return 600;
    }

    /**
     * Override to change default height
     *
     * @return scene's height
     */
    protected int getHeight() {
        return 600;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public final void initGame(Parent gameRoot) {

//        new Thread(() -> Application.launch(getClass())).start();
        changeScene(getGameScene());
//        GameGraphicView.waitForStartUp();
    }
}
