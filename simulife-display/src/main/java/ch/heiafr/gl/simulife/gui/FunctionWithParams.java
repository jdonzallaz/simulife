package ch.heiafr.gl.simulife.gui;

import ch.heiafr.gl.simulife.model.ICreature;

import java.util.function.BiConsumer;

/**
 * This class is used to create an object that stores a function reference and its parameters. In
 * this way, the function can be executed later.
 *
 * @author Jonathan Rial
 */
class FunctionWithParams {

  private BiConsumer<ICreature, Integer[]> function;
  private Object[] params;

  /**
   * Constructor of the FunctionWithParams class.
   *
   * @param function the function reference as a BiConsumer<ICreature, Integer[]> object.
   * @param params the function parameters as an array of objects.
   */
  FunctionWithParams(BiConsumer<ICreature, Integer[]> function, Object[] params) {
    this.function = function;
    this.params = params;
  }

  /**
   * Gets the function reference.
   *
   * @return the function reference as a BiConsumer<ICreature, Integer[]> object.
   */
  BiConsumer<ICreature, Integer[]> getFunction() {
    return function;
  }

  /**
   * Gets the function parameters.
   *
   * @return the function parameters as an array of objects.
   */
  Object[] getParams() {
    return params;
  }
}
