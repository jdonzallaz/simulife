package ch.heiafr.gl.simulife.gui;

import ch.heiafr.gl.simulife.model.ICreature;
import javafx.geometry.Point2D;

import java.util.HashMap;

/**
 * This class is used to create the text view of the game.
 *
 * @author Jonathan Rial
 */
public class GameTextView implements IGameView {

  private ICreature[][] grid;
  private HashMap<ICreature, Point2D> creatures;

  /** {@inheritDoc} */
  @Override
  public void initGame(int width, int height) {
    this.grid = new ICreature[width][height];
    this.creatures = new HashMap<>();
    System.out.println("Start game\n");
  }

  /** {@inheritDoc} */
  @Override
  public void set(ICreature creature, int x, int y) {
    try {
      grid[x][y] = creature;
      creatures.put(creature, new Point2D(x, y));
    } catch (ArrayIndexOutOfBoundsException e) {
      throw new RuntimeException("Adding a creature outside the game grid");
    }
  }

  /** {@inheritDoc} */
  @Override
  public void move(ICreature creature, int x, int y) {
    remove(creature);
    set(creature, x, y);
  }

  /** {@inheritDoc} */
  @Override
  public void remove(ICreature creature) {
    Point2D coordinates = creatures.get(creature);

    if (coordinates == null) {
      throw new RuntimeException("Creature not found");
    }

    grid[(int) coordinates.getX()][(int) coordinates.getY()] = null;
    creatures.remove(creature);
  }

  /** {@inheritDoc} */
  @Override
  public void refresh() {
    System.out.println("Displaying the game grid");

    int row;
    for (row = 0; row < grid[0].length; ++row) {
      int col;
      for (col = 0; col < grid.length; ++col) {
        System.out.print("+---+");
      }

      System.out.println();

      for (col = 0; col < grid.length; ++col) {
        ICreature creature = grid[col][row];

        if (creature != null) {
          System.out.print("| " + creature.getName() + " |");
        } else {
          System.out.print("| _ |");
        }
      }

      System.out.println();
    }

    for (row = 0; row < grid.length; ++row) {
      System.out.print("+---+");
    }

    System.out.println("\n");
  }
}
