---

```
public interface ICreature
```

This interface contains all required getters for a creature.

## Method Summary

| Modifier and Type | Method and Description |
| --- | --- |
| `java.lang.String` | `getName()` <br> Gets the name of the creature to display it in the text view of the game. |
| `javafx.scene.paint.Color` | `getColor()` <br> Gets the color of the creature to display it in the matrix view of the game. |
| `javafx.scene.image.Image` | `getImage()` <br> Gets the image of the creature to display it in the image view of the game. |

## Method Detail

### getName

`java.lang.String getName()`

Gets the name of the creature to display it in the text view of the game.

**Returns:**

the name of the creature.

---

### getColor

`javafx.scene.paint.Color getColor()`

Gets the color of the creature to display it in the matrix view of the game.

**Returns:**

the color of the creature.

---

### getImage

`javafx.scene.image.Image getImage()`

Gets the image of the creature to display it in the image view of the game.

**Returns:**

the image of the creature.