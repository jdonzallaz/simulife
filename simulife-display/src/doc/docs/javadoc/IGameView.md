**All Known Implementing Classes:**

[GameGraphicView](GameGraphicView.md), [GameMatrixView](GameMatrixView.md), [GameTextView](GameTextView.md)

---

```
public interface IGameView
```

This interface contains all required methods for all game views.

## Method Summary

| Modifier and Type | Method and Description |
| --- | --- |
| `void` | `init(int width, int height)` <br> This method initializes the game view with a specified grid size. |
| `void` | `set(ICreature creature, int x, int y)` <br> This method sets a creature on the grid at a specified cell. |
| `void` | `move(ICreature creature, int x, int y)` <br> This method moves a creature on the grid to a specified cell. |
| `void` | `remove(ICreature creature)` <br> This method removes a creature from the grid. |
| `void` | `refresh()` <br> This methods refreshes the game view. |

## Method Detail

### init

`void init(int width, int height)`

This method initializes the game view with a specified grid size.

**Parameters:**

* `width` - the width of the grid.
* `height` - the height of the grid.

---

### set

`void set(ICreature creature, int x, int y)`

This method sets a creature on the grid at a specified cell.

**Parameters:**

* `creature` - the creature to set on the grid.
* `x` - the index of the column.
* `y` - the index of the row.

---

### move

`void move(ICreature creature, int x, int y)`

This method moves a creature on the grid to a specified cell.

**Parameters:**

* `creature` - the creature to move on the grid.
* `x` - the index of the column.
* `y` - the index of the row.

---

### remove

`void remove(ICreature creature)`

This method removes a creature from the grid.

**Parameters:**

* `creature` - the creature to remove from the grid.

---

### refresh

`void refresh()`

This method sets a creature on the grid at a specified cell.