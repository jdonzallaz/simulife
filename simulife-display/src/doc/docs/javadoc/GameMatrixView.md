**All Implemented Interfaces:**

[IGameView](IGameView.md)

---

```
public class GameMatricView 
extends javafx.application.Application 
implements IGameView
```

This class is used to create the matrix view of the game.

## Constructor Summary

| Constructor and Description |
| --- |
| `GameMatrixView()` |

## Method Summary

| Modifier and Type | Method and Description |
| --- | --- |
| `void` | `start(javafx.stage.Stage primaryStage)` <br> This method creates the JavaFX view. |
| `void` | `init(int width, int height)` <br> This method initializes the game view with a specified grid size. |
| `void` | `set(ICreature creature, int x, int y)` <br> This method sets a creature on the grid at a specified cell. |
| `void` | `move(ICreature creature, int x, int y)` <br> This method moves a creature on the grid to a specified cell. |
| `void` | `remove(ICreature creature)` <br> This method removes a creature from the grid. |
| `void` | `refresh()` <br> This methods refreshes the game view. |

## Constructor Detail

### GameMatrixView

`public GameMatrixView()`

## Method Detail

### start

`public void start(javafx.stage.Stage primaryStage)`

This method creates the JavaFX view.

**Specified by:**

* `start` in class `javafx.application.Application`

**Parameters:**

* `primaryStage` - the primary stage, onto which the application scene can be set.

---

### init

`public void init(int width, int height)`

This method initializes the game view with a specified grid size.

**Specified by:**

* `init` in interface `IGameView`

**Parameters:**

* `width` - the width of the grid.
* `height` - the height of the grid.

---

### set

`public void set(ICreature creature, int x, int y)`

This method sets a creature on the grid at a specified cell.

**Specified by:**

* `set` in interface `IGameView`

**Parameters:**

* `creature` - the creature to set on the grid.
* `x` - the index of the column.
* `y` - the index of the row.

---

### move

`public void move(ICreature creature, int x, int y)`

This method moves a creature on the grid to a specified cell.

**Specified by:**

* `move` in interface `IGameView`

**Parameters:**

* `creature` - the creature to move on the grid.
* `x` - the index of the column.
* `y` - the index of the row.

---

### remove

`public void remove(ICreature creature)`

This method removes a creature from the grid.

**Specified by:**

* `remove` in interface `IGameView`

**Parameters:**

* `creature` - the creature to remove from the grid.

---

### refresh

`public void refresh()`

This method sets a creature on the grid at a specified cell.

**Specified by:**

* `refresh` in interface `IGameView`