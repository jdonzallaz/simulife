# Welcome to Simulife Display Library

This library provides you 3 different graphical interfaces for the Simulife game:

![3 different graphical interfaces](img/3views.png)