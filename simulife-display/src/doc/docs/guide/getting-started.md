## Create your creatures

All classes in your application that represent creatures must implement the [ICreature](../javadoc/ICreature.md) interface. This interface describes three methods:

* `getName()`: returns the name of the creature to display it in the text view of the game
* `getColor()`: returns the color of the creature to display it in the matrix view of the game
* `getImage()`: returns the image of the creature to display it in the image view of the game

### Example

```java
public class MyCreatureA implements ICreature {

  @Override
  public String getName() {
    return "A";
  }

  @Override
  public Color getColor() {
    return Color.RED;
  }

  @Override
  public Image getImage() {
    return new Image("a.gif");
  }
}
```

## Create the user interface

To start, you need to choose the implementation you want between [GameTextView](../javadoc/GameTextView.md), [GameMatrixView](../javadoc/GameMatrixView.md) and [GameGraphicView](../javadoc/GameGraphicView.md).

```java
IGameView gameView = new GameGraphicView();
```

Then, initialize it to the size you want:

```java
gameView.init(6, 6);
```

These implementations provide several methods for manipulating creatures on the grid:

* `set(ICreature creature, int x, int y)`: sets a creature on the grid at a specified cell
* `move(ICreature creature, int x, int y)`: moves a creature on the grid to a specified cell
* `remove(ICreature creature)`: removes a creature from the grid

Finally, to display the new state of the game, just call the method `refresh()`.

### Example

```java
public class App {

  public static void main(String[] args) {

    IGameView gameView = new GameGraphicView();
    gameView.init(6, 6);

    ICreature a = new MyCreatureA();

    gameView.set(a, 1, 1);
    gameView.refresh();
  }
}
```

## Download the sample project

You can download the sample project [here](../files/simulife-example.zip).

This project uses gradle. To execute it, run `gradle run` in the terminal.