# Installation

## Maven

To start, it is necessary to define the custom repository. To do this, you must add the following code to your `pom.xml` file:

```xml
<repositories>
   <repository>
      <id>gl-repo</id>
      <name>Repo for GL courses</name>
      <url>http://simulife.tic.heia-fr.ch/maven</url>
   </repository>
</repositories>
```

Finally, just add the library to the dependencies of your project:

```xml
<dependency>
   <groupId>ch.heiafr.gl</groupId>
   <artifactId>simulife-display</artifactId>
   <version>1.0</version>
   <scope>compile</scope>
</dependency>
```

## Gradle
To start, it is necessary to define the custom repository. To do this, you must add the following code to your `build.gradle` file:

```groovy
repositories {
   mavenCentral()

   maven {
      url "http://simulife.tic.heia-fr.ch/maven"
   }
}
```

Finally, just add the library to the dependencies of your project:

```groovy
dependencies {
   compile "ch.heiafr.gl:simulife-display:1.0"
}
```