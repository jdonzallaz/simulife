package ch.heiafr.gl.simulife.example;

import ch.heiafr.gl.simulife.gui.GameGraphicView;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.layout.GridPane;

import java.io.IOException;

/**
 * This class extends GameGraphicView to modify the root.
 *
 * @author Jonathan Donzallaz
 */
public class Display extends GameGraphicView {
    @Override
    public Parent getGameRoot() {
        // Load FXML
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("display.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }

        GridPane grid = (GridPane) loader.getNamespace().get("mainGrid");
        setGridPane(grid);

        return root;
    }
}
