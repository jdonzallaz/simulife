package ch.heiafr.gl.simulife.example;

import ch.heiafr.gl.simulife.gui.GameGrid;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;

import java.io.IOException;

public class Test extends Application {
    private GameGrid grid;

    @Override
    public void start(Stage primaryStage) {

        // Load FXML
        FXMLLoader loader = new FXMLLoader(getClass().getClassLoader().getResource("display.fxml"));
        Parent root = null;
        try {
            root = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
            return;
        }

        grid = (GameGrid) loader.getNamespace().get("mainGrid");
        grid.init(8, 8);
        primaryStage.setScene(new Scene(root, 600, 600));
        primaryStage.show();
    }
}
