package ch.heiafr.gl.simulife.example.creature;

import ch.heiafr.gl.simulife.model.ICreature;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class MyCreatureA implements ICreature {

  @Override
  public String getName() {
    return "A";
  }

  @Override
  public Color getColor() {
    return Color.RED;
  }

  @Override
  public Image getImage() {
    return new Image("a.gif");
  }
}
