package ch.heiafr.gl.simulife.example.creature;

import ch.heiafr.gl.simulife.model.ICreature;
import javafx.scene.image.Image;
import javafx.scene.paint.Color;

public class MyCreatureB implements ICreature {

  @Override
  public String getName() {
    return "B";
  }

  @Override
  public Color getColor() {
    return Color.BLUE;
  }

  @Override
  public Image getImage() {
    return new Image("b.gif");
  }
}
