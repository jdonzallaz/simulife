package ch.heiafr.gl.simulife.example;

import ch.heiafr.gl.simulife.example.creature.MyCreatureA;
import ch.heiafr.gl.simulife.example.creature.MyCreatureB;
import ch.heiafr.gl.simulife.gui.IGameView;
import ch.heiafr.gl.simulife.model.ICreature;
import javafx.application.Application;

public class App {

  public static void main(String[] args) {

    // IGameView gameView = new GameTextView();
    // IGameView gameView = new GameMatrixView();
    IGameView gameView = new Display();
    new Thread(() -> Application.launch(gameView.getClass())).start();
    gameView.initGame(6, 6);

    ICreature a = new MyCreatureA();
    ICreature b = new MyCreatureB();

    gameView.set(a, 1, 1);
    gameView.refresh();

    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    gameView.set(b, 2, 4);
    gameView.move(a, 2, 1);
    gameView.refresh();

    try {
      Thread.sleep(1000);
    } catch (InterruptedException e) {
      e.printStackTrace();
    }

    gameView.move(b, 2, 5);
    gameView.remove(a);
    gameView.refresh();
  }
}
